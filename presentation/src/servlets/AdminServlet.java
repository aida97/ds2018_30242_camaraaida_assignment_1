package servlets;

import servicies.FlightServiceImpl;
import vos.CityIdVo;
import vos.CityVo;
import vos.FlightVo;
import vos.TypeVo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Pattern;


@WebServlet(name = "AdminServlet")
public class AdminServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ///create flight
        response.setContentType("text/html");
        FlightServiceImpl service = new FlightServiceImpl();
        String type = request.getParameter("type");
        TypeVo typeEnum = type.equals("BIG")?TypeVo.BIG:TypeVo.SMALL;
        String deptLong = request.getParameter("depLong");
        String deptLat = request.getParameter("depLat");
        String arrLong = request.getParameter("arrLong");
        String arrLat = request.getParameter("arrLat");
        String depDate = request.getParameter("depDate");
        String arrDate = request.getParameter("arrDate");
        String pattern = "([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01])):[0-9][0-9]:[0-9][0-9]";
        //boolean typeOk = type.equals("BIG") || type.equals("SMALL");
        boolean depOk = Pattern.matches(pattern, depDate);
        boolean arrOk = Pattern.matches(pattern, arrDate);
        boolean datesOk = depOk && arrOk;
        if (datesOk) {
            FlightVo flight = new FlightVo(typeEnum,
                    new CityVo(new CityIdVo(deptLong, deptLat)),
                    new CityVo(new CityIdVo(arrLong, arrLat)),
                    depDate,
                    arrDate);
            FlightVo flightVo = service.addFlight(flight);
            PrintWriter out = response.getWriter();
            String docType =
                    "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
            String htmlPage = docType+"<html>\n" +
                    "<head><title>" + "Administrator" + "</title></head>\n" +
                    "<body bgcolor = \"#f0f0f0\">\n" +
                    "<table><th>Number</th><th>Type</th>" +
                    "<th>Departure City Longitude</th>" +
                    "<th>Departure City Latitude</th>" +
                    "<th>Arrival City Longitude</th>" +
                    "<th>Arrival City Latitude</th>" +
                    "<th>Departure Date</th>" +
                    "<th>Arrival Date</th>" +
                    "<tr>" +
                    "<td>" +flightVo.getNumber()+"</td>"+
                    "<td>" +flightVo.getTypeVo()+"</td>"+
                    "<td>" +flightVo.getDepartureCity().getId().getLongitude()+"</td>"+
                    "<td>" +flightVo.getDepartureCity().getId().getLatitude()+"</td>"+
                    "<td>" +flightVo.getArrivalCity().getId().getLongitude()+"</td>"+
                    "<td>" +flightVo.getArrivalCity().getId().getLatitude()+"</td>"+
                    "<td>" +flightVo.getDepartureDate()+"</td>"+
                    "<td>" +flightVo.getArrivalDate()+"</td>"+
                    "<tr></table></body></html>";
            out.println(htmlPage);
        } else {
            PrintWriter out = response.getWriter();
            String docType =
                    "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
            String htmlPage = docType+"<html>\n" +
                    "<head><title>" + "Administrator" + "</title></head>\n" +
                    "<body bgcolor = \"#f0f0f0\">\n" +
                    "<h1>Invalid dates or type</h1>"+
                    "</body></html>";
            out.println(htmlPage);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //view flight
        response.setContentType("text/html");

        Long number = Long.parseLong(request.getParameter("number"));
        FlightServiceImpl flightService = new FlightServiceImpl();
        FlightVo flightVo = flightService.getFlight(number);
        PrintWriter out = response.getWriter();
        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
        if (flightVo != null) {
            String htmlPage = docType+"<html>\n" +
                    "<head><title>" + "Administrator" + "</title></head>\n" +
                    "<body bgcolor = \"#f0f0f0\">\n" +
                    "<table><th>Number</th><th>Type</th>" +
                    "<th>Departure City Longitude</th>" +
                    "<th>Departure City Latitude</th>" +
                    "<th>Arrival City Longitude</th>" +
                    "<th>Arrival City Latitude</th>" +
                    "<th>Departure Date</th>" +
                    "<th>Arrival Date</th>" +
                    "<tr>" +
                    "<td>" + flightVo.getNumber()+"</td>"+
                    "<td>" +flightVo.getTypeVo()+"</td>"+
                    "<td>" +flightVo.getDepartureCity().getId().getLongitude()+"</td>"+
                    "<td>" +flightVo.getDepartureCity().getId().getLatitude()+"</td>"+
                    "<td>" +flightVo.getArrivalCity().getId().getLongitude()+"</td>"+
                    "<td>" +flightVo.getArrivalCity().getId().getLatitude()+"</td>"+
                    "<td>" +flightVo.getDepartureDate()+"</td>"+
                    "<td>" +flightVo.getArrivalDate()+"</td>"+
                    "<tr></table></body></html>";
            out.println(htmlPage);
        } else {
            String htmlPage = docType+"<html>\n" +
                    "<head><title>" + "Administrator" + "</title></head>\n" +
                    "<body bgcolor = \"#f0f0f0\">\n" +
                    "<h1>Not Found</h1></body></html>";
            out.println(htmlPage);
        }

    }
    public void init() throws ServletException {

    }


    public void destroy() {
        // do nothing.
    }

}
