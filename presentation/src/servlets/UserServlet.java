package servlets;

import servicies.FlightServiceImpl;
import vos.FlightVo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


@WebServlet(name = "UserServlet")
public class UserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        FlightServiceImpl service = new FlightServiceImpl();
        List<FlightVo> flights = service.getFlights();
        String table = "<table><th>Number</th><th>Type</th>" +
                "<th>Departure City Longitude</th>" +
                "<th>Departure City Latitude</th>" +
                "<th>Arrival City Longitude</th>" +
                "<th>Arrival City Latitude</th>" +
                "<th>Departure Date</th>" +
                "<th>Arrival Date</th>";
        for(FlightVo flightVo:flights){
            table = table.concat("<tr>");

            table = table.concat("<td>");
            table = table.concat(flightVo.getNumber().toString());
            table = table.concat("</td>");

            table = table.concat("<td>");
            table = table.concat(flightVo.getTypeVo().name());
            table = table.concat("</td>");

            table = table.concat("<td>");
            table = table.concat(flightVo.getDepartureCity().getId().getLongitude());
            table = table.concat("</td>");

            table = table.concat("<td>");
            table = table.concat(flightVo.getDepartureCity().getId().getLatitude());
            table = table.concat("</td>");

            table = table.concat("<td>");
            table = table.concat(flightVo.getArrivalCity().getId().getLongitude());
            table = table.concat("</td>");

            table = table.concat("<td>");
            table = table.concat(flightVo.getArrivalCity().getId().getLatitude());
            table = table.concat("</td>");

            table = table.concat("<td>");
            table = table.concat(flightVo.getDepartureDate());
            table = table.concat("</td>");

            table = table.concat("<td>");
            table = table.concat(flightVo.getArrivalDate());
            table = table.concat("</td>");

            table = table.concat("</tr>");
        }
        table = table.concat("</table>");
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
        String htmlPage = docType+"<html>\n" +
                "<head><title>" + "User" + "</title></head>\n" +
                "<body bgcolor = \"#f0f0f0\">\n" +
                "<h1" + "All flights" + "</h1>\n"+
                table +"</body></html>";
        out.println(htmlPage);

    }
}
