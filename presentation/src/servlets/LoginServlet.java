package servlets;

import servicies.LoginService;
import vos.RoleVo;
import vos.UserVo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        LoginService loginService = new LoginService();
        UserVo user = loginService.findUser(username,password);
       // UserVo user = new UserVo("admin","password",RoleVo.ADMIN);
        HttpSession session = request.getSession(true);
        session.setAttribute("user", user);
        if(user == null) {
            String url = request.getContextPath() + "/login.html";
            response.sendRedirect(url);
        } else if (user.getRole() == RoleVo.ADMIN) {
            String url = request.getContextPath() + "/administratorPage";
            response.sendRedirect(url);
        } else {
            String url = request.getContextPath() + "/userPage";
            response.sendRedirect(url);
        }
    }


}
