package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "AdminFormsServlet")
public class AdminFormsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
        String htmlPage = docType+ "<html>\n" +
                "<head><title>" + "Administrator" + "</title></head>\n" +
                "<body bgcolor = \"#f0f0f0\">\n" +
                "<h1>" + "Add new flight" + "</h1>\n" +
                "<form method=\"post\" action=\"administrator\">\n" +
                "    <p><label>Type</label><input type=\"text\" name=\"type\"/></p>\n" +
                "    <p><label>departureCityLongitude</label><input type=\"text\" name=\"depLong\"/></p>\n" +
                "    <p><label>departureCityLatitude</label><input type=\"text\" name=\"depLat\"/></p>\n" +
                "    <p><label>arrivalCityLongitude</label><input type=\"text\" name=\"arrLong\"/></p>\n" +
                "    <p><label>arrivalCityLatitude</label><input type=\"text\" name=\"arrLat\"/></p>\n" +
                "    <p><label>departureDate(yyyy-mm-dd hh:mm)</label><input type=\"text\" name=\"depDate\"/></p>\n" +
                "    <p><label>arrivalDate(yyyy-mm-dd hh:mm)</label><input type=\"text\" name=\"arrDate\"/></p>\n" +
                "    <p><button type=\"submit\">create flight</button></p>\n" +
                "</form>\n" +
                "\n" +
                "\n" +
                "<h1>Delete flight</h1>\n" +
                "<form method=\"post\" action=\"administratorDelete\">\n" +
                "    <p><label>Number</label><input type=\"text\" name=\"number\"/></p>\n" +
                "    <p><button type=\"submit\">delete flight</button></p>\n" +
                "</form>\n" +
                "\n" +
                "\n" +
                "<h1>View flight</h1>\n" +
                "<form method=\"get\" action=\"administrator\" >\n" +
                "    <p><label>Number</label><input type=\"text\" name=\"number\"/></p>\n" +
                "    <p><button type=\"submit\">View</button></p>\n" +
                "</form>\n" +
                "<h1>Update flight</h1>\n" +
                "<form method=\"get\" action=\"administratorDelete\" >\n" +
                "    <p><label>Number</label><input type=\"text\" name=\"number\"/></p>\n" +
                "<p><label>Departure date(dd-mm-yyyy:hh:mm)</label><input type=\"text\" name=\"depDate\"></p>\n" +
                "    <p><button type=\"submit\">View</button></p>\n" +
                "</form>\n" +
                "</body>\n" +
                "</html>";
        out.println(htmlPage);

    }
}
