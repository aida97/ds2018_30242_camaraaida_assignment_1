package servlets;

import servicies.FlightServiceImpl;
import vos.FlightVo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Pattern;


@WebServlet(name = "AdminSecondServlet")
public class AdminSecondServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //delete flight
        response.setContentType("text/html");
        Long number = Long.parseLong(request.getParameter("number"));
        FlightServiceImpl flightService = new FlightServiceImpl();
        boolean success = flightService.deleteFlight(number);
        PrintWriter out = response.getWriter();
        String ok = success?"deleted":"something bad happened";
        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
        String htmlPage = docType+"<html>\n" +
                "<head><title>" + "Administrator" + "</title></head>\n" +
                "<body><h1>" + ok +"</h1>"+
                "</body></html>";
        out.println(htmlPage);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//update flight
        response.setContentType("text/html");
        Long number = Long.parseLong(request.getParameter("number"));
        String date = request.getParameter("depDate");
        String pattern = "([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01])):[0-9][0-9]:[0-9][0-9]";
        boolean depOk = Pattern.matches(pattern, date);
        PrintWriter out = response.getWriter();
        if(depOk) {
            FlightServiceImpl flightService = new FlightServiceImpl();
            boolean success = flightService.updateFlight(number, date);
            String ok = success ? "updated" : "something bad happened";
            String docType =
                    "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
            String htmlPage = docType + "<html>\n" +
                    "<head><title>" + "Administrator" + "</title></head>\n" +
                    "<body><h1>" + ok + "</h1>" +
                    "</body></html>";
            out.println(htmlPage);
        } else {
            String docType =
                    "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
            String htmlPage = docType + "<html>\n" +
                    "<head><title>" + "Administrator" + "</title></head>\n" +
                    "<body><h1>Incorrect date</h1>" +
                    "</body></html>";
            out.println(htmlPage);
        }
    }
}
