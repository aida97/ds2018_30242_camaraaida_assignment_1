package filters;

import vos.RoleVo;
import vos.UserVo;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebFilter(filterName = "AdminFilter")
public class AdminFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpSession session = request.getSession(false);

        boolean loggedIn = session != null && session.getAttribute("user") != null;

        String loginURI = request.getContextPath() + "/login.html";

        if (loggedIn) {
            UserVo user = (UserVo) session.getAttribute("user");
            boolean isAdmin = user.getRole() == RoleVo.ADMIN;
            if(isAdmin){
                chain.doFilter(request, response);
            } else {
                response.sendRedirect(loginURI);
            }
        } else {
            response.sendRedirect(loginURI);
        }

    }

    public void init(FilterConfig config) throws ServletException {

    }

}
