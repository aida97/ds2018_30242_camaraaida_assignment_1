package mappers;


import entities.*;
import vos.*;

public class Mapper {
    public FlightVo map(Flight flight) {

        return new FlightVo(flight.getNumber(), map(flight.getType()), map(flight.getDepartureCity()),
                map(flight.getArrivalCity()), flight.getDepartureDate(), flight.getArrivalDate());
    }
    public UserVo map(User user){
        return new UserVo(user.getUsername(), user.getPassword(), map(user.getRole()));
    }
    private CityIdVo map(CityId cityId){
        return new CityIdVo(cityId.getLongitude(), cityId.getLatitude());
    }
    private CityVo map(City city){
        CityIdVo idVo = map(city.getId());
        return new CityVo(idVo);
    }
    private TypeVo map(Type type){
        switch (type){
            case BIG: return TypeVo.BIG;
            default: return TypeVo.SMALL;
        }
    }
    private RoleVo map(Role role){
        switch (role){
            case ADMIN: return RoleVo.ADMIN;
            default: return RoleVo.CLIENT;
        }
    }

    private CityId map(CityIdVo idVo){
        return new CityId(idVo.getLongitude(), idVo.getLatitude());
    }

    private City map(CityVo cityVo){
        return new City(map(cityVo.getId()));
    }
    private Type map(TypeVo type){
        switch (type){
            case BIG: return Type.BIG;
            default: return Type.SMALL;
        }
    }
    public Role map(RoleVo role){
        switch (role){
            case ADMIN: return Role.ADMIN;
            default: return Role.CLIENT;
        }
    }
    public Flight map(FlightVo flightVo){
        return new Flight(map(flightVo.getTypeVo()),
                map(flightVo.getDepartureCity()), map(flightVo.getArrivalCity()),
                        flightVo.getDepartureDate(), flightVo.getArrivalDate());
    }
}
