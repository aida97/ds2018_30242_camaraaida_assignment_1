package servicies;

import entities.Flight;
import mappers.Mapper;
import operations.FlightDao;
import vos.FlightVo;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


public class FlightServiceImpl {
    private FlightDao flightDao;
    private Mapper mapper;

    public FlightServiceImpl() {
        flightDao = new FlightDao();
        mapper = new Mapper();
    }

    public List<FlightVo> getFlights() {
        return flightDao.findFlights().stream().map(flight -> mapper.map(flight)).collect(Collectors.toList());
    }

    public FlightVo getFlight(long number) {
        Flight flight = flightDao.findFlight(number);
        if(flight != null){
            return mapper.map(flight);
        } else {
            return null;
        }

    }

    public boolean deleteFlight(long number) {
        Flight foundFlight = flightDao.findFlight(number);
        return foundFlight != null && flightDao.deleteFlight(number);
    }

    public FlightVo addFlight(FlightVo flightVo){
        return  mapper.map(flightDao.addFlight(mapper.map(flightVo)));
    }

    public boolean updateFlight(Long number, String date) {

        Flight foundFlight = flightDao.findFlight(number);
        return foundFlight != null && flightDao.updateFlight(number, date);
    }
}
