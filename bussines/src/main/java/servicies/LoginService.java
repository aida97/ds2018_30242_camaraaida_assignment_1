package servicies;

import entities.User;
import mappers.Mapper;
import operations.UserDao;
import vos.UserVo;


public class LoginService {
    private UserDao userDao = new UserDao();
    private Mapper mapper;
    public LoginService(){
        mapper = new Mapper();
    }
    public UserVo findUser(String username, String password) {
        User user = userDao.findUser(username,password);
        if(user != null){
            return mapper.map(user);
        } else {
            return null;
        }

    }
}
