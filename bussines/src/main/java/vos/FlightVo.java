package vos;


import java.util.Date;

public class FlightVo {
    private Long number;
    private TypeVo type;
    private CityVo departureCity;
    private CityVo arrivalCity;
    private String departureDate;
    private String arrivalDate;

    public FlightVo() {
    }

    public FlightVo(Long number, TypeVo type, CityVo departureCity, CityVo arrivalCity, String departureDate, String arrivalDate) {
        this.number = number;
        this.type = type;
        this.departureCity = departureCity;
        this.arrivalCity = arrivalCity;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
    }

    public FlightVo(TypeVo type, CityVo departureCity, CityVo arrivalCity, String departureDate, String arrivalDate) {
        this.type = type;
        this.departureCity = departureCity;
        this.arrivalCity = arrivalCity;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public TypeVo getTypeVo() {
        return type;
    }

    public void setTypeVo(TypeVo type) {
        this.type = type;
    }

    public CityVo getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(CityVo departureCity) {
        this.departureCity = departureCity;
    }

    public CityVo getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(CityVo arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }
}
