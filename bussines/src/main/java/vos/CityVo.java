package vos;



public class CityVo {
    private CityIdVo id;

    public CityVo(CityIdVo id) {
        this.id = id;
    }

    public CityIdVo getId() {
        return id;
    }

    public void setId(CityIdVo id) {
        this.id = id;
    }
}
