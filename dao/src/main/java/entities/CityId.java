package entities;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class CityId implements Serializable{

    @Column(name="longitude")
    private String longitude;

    @Column(name="latitude")
    private String latitude;

    public CityId() {
    }

    public CityId(String longitude, String latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CityId cityId = (CityId) o;

        if (longitude != null ? !longitude.equals(cityId.longitude) : cityId.longitude != null) return false;
        return latitude != null ? latitude.equals(cityId.latitude) : cityId.latitude == null;
    }

    @Override
    public int hashCode() {
        int result = longitude != null ? longitude.hashCode() : 0;
        result = 31 * result + (latitude != null ? latitude.hashCode() : 0);
        return result;
    }
}
