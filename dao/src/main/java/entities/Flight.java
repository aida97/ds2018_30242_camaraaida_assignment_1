package entities;

import javax.persistence.*;
import java.util.Date;

@Entity(name="Flight")
@Table(name="new_table")
public class Flight {

    @Column(name="number")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long number;

    @Column(name="type")
    private Type type;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(
                    name = "departureCityLongitude",
                    referencedColumnName = "longitude"),
            @JoinColumn(
                    name = "departureCityLatitude",
                    referencedColumnName = "latitude")
    })
    private City departureCity;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(
                    name = "arrivalCityLongitude",
                    referencedColumnName = "longitude"),
            @JoinColumn(
                    name = "arrivalCityLatitude",
                    referencedColumnName = "latitude")
    })
    private City arrivalCity;

    @Column(name = "departureDate")
    private String departureDate;

    @Column(name="arrivalDate")
    private String arrivalDate;

    public Flight() {
    }

    public Flight(Type type, City departureCity, City arrivalCity, String departureDate, String arrivalDate) {
        this.type = type;
        this.departureCity = departureCity;
        this.arrivalCity = arrivalCity;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
    }

    public Flight(Long number, Type type, City departureCity, City arrivalCity, String departureDate, String arrivalDate) {
        this.number = number;
        this.type = type;
        this.departureCity = departureCity;
        this.arrivalCity = arrivalCity;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public City getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(City departureCity) {
        this.departureCity = departureCity;
    }

    public City getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(City arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Flight flight = (Flight) o;

        if (number != flight.number) return false;
        if (type != flight.type) return false;
        if (departureCity != null ? !departureCity.equals(flight.departureCity) : flight.departureCity != null)
            return false;
        if (arrivalCity != null ? !arrivalCity.equals(flight.arrivalCity) : flight.arrivalCity != null) return false;
        if (departureDate != null ? !departureDate.equals(flight.departureDate) : flight.departureDate != null)
            return false;
        return arrivalDate != null ? arrivalDate.equals(flight.arrivalDate) : flight.arrivalDate == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (number ^ (number >>> 32));
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (departureCity != null ? departureCity.hashCode() : 0);
        result = 31 * result + (arrivalCity != null ? arrivalCity.hashCode() : 0);
        result = 31 * result + (departureDate != null ? departureDate.hashCode() : 0);
        result = 31 * result + (arrivalDate != null ? arrivalDate.hashCode() : 0);
        return result;
    }
}
