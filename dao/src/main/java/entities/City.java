package entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity(name="City")
@Table(name="city")
public class City {

    @EmbeddedId
    private CityId id;

    public City() {
    }

    public City(CityId cityId) {
        id=cityId;
    }

    public CityId getId() {
        return id;
    }

    public void setId(CityId id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        City city = (City) o;

        return id != null ? id.equals(city.id) : city.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
