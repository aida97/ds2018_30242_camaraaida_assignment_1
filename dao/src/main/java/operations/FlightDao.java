package operations;

import entities.Flight;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FlightDao {
    private static final Log LOGGER = LogFactory.getLog(FlightDao.class);

    private SessionFactory factory;

    public FlightDao() {

        try {
            factory = new Configuration().configure().addAnnotatedClass(Flight.class).buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public Flight addFlight(Flight flight) {
        long flightId = -1;
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            flightId = (Long) session.save(flight);
            flight.setNumber(flightId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flight;
    }

    public List<Flight> findFlights() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> flights = new ArrayList<Flight>();
        try {
            tx = session.beginTransaction();
            flights = session.createQuery("SELECT f FROM Flight f", Flight.class).list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flights;
    }

    public Flight findFlight(long id) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("SELECT f FROM Flight f WHERE f.number = :id", Flight.class);
            query.setParameter("id", id);
            flights = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flights != null && !flights.isEmpty() ? flights.get(0) : null;

    }

    public boolean deleteFlight(long id) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("delete from Flight f where f.number = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            tx.commit();
            return true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return false;
    }

    public boolean updateFlight(Long number, String date) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("update Flight f set f.departureDate = :date where f.number = :id");
            query.setParameter("id", number).setParameter("date", date);
            query.executeUpdate();
            tx.commit();
            return true;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
            return false;
        } finally {
            session.close();
        }

    }
}
