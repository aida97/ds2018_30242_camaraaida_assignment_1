package operations;


import entities.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class UserDao {
    private static final Log LOGGER = LogFactory.getLog(UserDao.class);
    private SessionFactory factory;

    public UserDao(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            factory = new Configuration().configure().buildSessionFactory();
        } catch (HibernateException e) {
            System.err.println("Failed to create sessionFactory object." + e);
            throw new ExceptionInInitializerError(e);
        } catch (ClassNotFoundException e) {
            System.err.println("Failed to create sessionFactory object." + e);
            throw new ExceptionInInitializerError(e);
        }
    }

    public User findUser(String username, String password){
        Session session = factory.openSession();
        Transaction tx = null;
        User user = null;
        try {
            tx = session.beginTransaction();
            List<User> users = session.createQuery("select u from User u where u.username=:username", User.class)
                    .setParameter("username",username)
                    .list();
            if(users.size()>0 && users.get(0).getPassword().equals(password)){
                user = users.get(0);
            }
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return user;
    }


}
