# Read me

Pentru a rula aplicatia trebuie sa urmati urmatorii pasi:

1. instalati serverul tomcat versiunea 7.0.91
2. asigurati-va ca serverul tomcat ruleaza pe portul 8080
2. adaugati in directorul de la adresa <director-instalare-tomcat>/lib fisierul jar care contine un driver mySql
3. instalati un server MySql
4. asigurati-va ca user-ul serverului este root si are parola root
5. asigurati-va ca serverul MySql ruleaza pe portul 3306
4. importati in server schema bazei de date
5. instalati maven versiunea 3.0.9 si adaugati calea la PATH
6. descarcati proiectul
7. in terminal, navigati in folderul radacina al proiectului
8. executati comanda mvn clean install
9. in terminal, navigati la <director-instalare-tomcat>/bin/startup.bat
10. intrati in browser la adresa http://localhost:8080/login.html